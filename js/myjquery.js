
var arrOfCheckedImage = [];
var cropper;
var originalImg;
$(document).ready(function() {
  var arr = ['images/1.jpg', 'images/1.jpg', 'images/1.jpg', 'images/1.jpg', 'images/1.jpg', 'images/1.jpg', 'images/1.jpg', 'images/1.jpg'];
  for (var i = 0; i < arr.length; i++) {
  $('#whatever').append("<div class='classImgGallery'><div class='img-container' style='overflow-y:hidden;background-image:url(" + arr[i] + ");background-repeat:no-repeat;background-size:contain;width:240px;height:240px;'></div></div>");
  }
  // btn visible on hover
  $('#fullpage').fullpage();
  $('#whatever').hoverGrid();

  $(document).on('click', '.classImgGallery div', function() {
    $(this).toggleClass("img-style-one");
  });

  //adding the action on CREATE PORTFOLIO button
  $(document).on('click', '#moveRight', function() {
    if ($('.classImgGallery div').hasClass("img-style-one")) {
      arrOfCheckedImage = new Array();
      $('.classImgGallery .img-style-one').each(function(item, el) {
        arrOfCheckedImage.push($(el).attr("src"));
      });
      $.fn.fullpage.moveSlideRight();
        initGridStack();
    } else {
      alert("Please select atleast ONE image");
    }
  });

  //adding the action on BACK TO GALLERY button
  $(document).on('click', '#moveLeft', function() {
    $.fn.fullpage.moveSlideRight();
  });
});

function initGridStack() {
  $('.grid-stack').gridstack({
    always_show_resize_handle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
    resizable: {
      handles: 'se,sw'
    }
  });

  loadGridFromSerializationArra();

  $(".thumbBox").resizable().draggable();

  var options = {
    imageBox: '.imageBox',
    thumbBox: '.thumbBox',
    spinner: '.spinner',
    imgSrc: 'avatar.png'
  }

  $('.grid-stack-item-content .nothing').click(showModal);

  function showModal() {
    originalImg = $(this).parent('div');
    $('#modal').on('shown.bs.modal', function(e) {
      $(this).find('.modal-body').css({
        width: 'auto', //probably not needed
        height: 'auto', //probably not needed
        'max-height': '100%'
      });
      var img = $(originalImg).find('img').attr('src');
      options.imgSrc = img;
      cropper = new cropbox(options);
    });
  }

  $(document).on('click', '#btnCrop', function() {
    var img = cropper.getDataURL();
    $(originalImg).css('background-image', 'url(' + img + ')');
    $(originalImg).css('background-repeat', 'no-repeat');
  });

  document.querySelector('#btnZoomIn').addEventListener('click', function() {
    cropper.zoomIn();
  });

  document.querySelector('#btnZoomOut').addEventListener('click', function() {
    cropper.zoomOut();
  });

  $('.grid-stack-item-content').hover(function() {
    $(this).find('button').css('display', 'inline-block');
  }, function() {
    $(this).find('button').css('display', 'none');
  });
}

$(document).on('click', '#btnSave', function() {
  var imageName = prompt("Please enter name for you Collage");
  if (imageName && imageName.length > 0) {
    html2canvas($('.grid-stack'), {
      useCORS: true,
      onrendered: function(canvas) {
        if ($('#export-image-container').length == 0) {
          $('body').append('<a id="export-image-container" download=' + imageName + '.jpg>');
          img = canvas.toDataURL("image/jpeg");
          img = img.replace('data:image/jpeg;base64,', '');
          finalImageSrc = 'data:image/jpeg;base64,' + img;

          $('#export-image-container').attr('href', finalImageSrc);
          $('#export-image-container')[0].click();
          $('#export-image-container').remove();
        }
      }
    });
  } else {
    alert("Please enter image name");
  }
});

function loadGridFromSerializationArra() {
  var serialization = [{
    x: 0,
    y: 0,
    width: 2,
    height: 2
  }, {
    x: 2,
    y: 0,
    width: 2,
    height: 2
  }, {
    x: 4,
    y: 0,
    width: 2,
    height: 2
  }, {
    x: 6,
    y: 0,
    width: 2,
    height: 2
  }, {
    x: 8,
    y: 0,
    width: 2,
    height: 2
  }, {
    x: 10,
    y: 0,
    width: 2,
    height: 2
  }, {
    x: 0,
    y: 2,
    width: 2,
    height: 2
  }, {
    x: 2,
    y: 2,
    width: 2,
    height: 2
  }, {
    x: 4,
    y: 2,
    width: 2,
    height: 2
  }, {
    x: 6,
    y: 2,
    width: 2,
    height: 2
  }, {
    x: 8,
    y: 2,
    width: 2,
    height: 2
  }, {
    x: 10,
    y: 2,
    width: 2,
    height: 2
  }, {
    x: 0,
    y: 4,
    width: 2,
    height: 2
  }, {
    x: 2,
    y: 4,
    width: 2,
    height: 2
  }, {
    x: 4,
    y: 4,
    width: 2,
    height: 2
  }, {
    x: 6,
    y: 4,
    width: 2,
    height: 2
  }, {
    x: 8,
    y: 4,
    width: 2,
    height: 2
  }, {
    x: 10,
    y: 4,
    width: 2,
    height: 2
  }, {
    x: 0,
    y: 6,
    width: 2,
    height: 2
  }, {
    x: 2,
    y: 6,
    width: 2,
    height: 2
  }, {
    x: 4,
    y: 6,
    width: 2,
    height: 2
  }, {
    x: 6,
    y: 6,
    width: 2,
    height: 2
  }, {
    x: 8,
    y: 6,
    width: 2,
    height: 2
  }, {
    x: 10,
    y: 6,
    width: 2,
    height: 2
  }, ];

  serialization = GridStackUI.Utils.sort(serialization);

  var grid = $('.grid-stack').data('gridstack');
  grid.remove_all();
  var i = 0;
  _.each(serialization, function(node) {
    if (i < arrOfCheckedImage.length) {
      grid.add_widget($('<div><div class="grid-stack-item-content" style="overflow-y:hidden;background-image:url(' + arrOfCheckedImage[i] + ');background-repeat:no-repeat;background-size:contain"><div class="nothing"><button style="float:right;display:none;border:none;" data-toggle="modal" data-target="#modal"><span class="fa fa-pencil fa-fw"/></button><img src = ' + arrOfCheckedImage[i] + ' style="display:none"></div></div></div>'),
        node.x, node.y, node.width, node.height);
        console.log(arrOfCheckedImage);
    }
    ++i;
  });
}
